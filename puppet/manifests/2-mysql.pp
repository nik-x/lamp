class { 'mysql::server':
  root_group  => 'vagrant',
  mysql_group => 'vagrant',
  mycnf_owner => 'vagrant',
  mycnf_group => 'vagrant',
  restart     => true,
  users       => {
    'vagrant@%' => {
      ensure        => present,
      password_hash => mysql::password('vagrant'),
    }
  },
  grants      => {
    'vagrant@%/*.*' => {
      user       => 'vagrant@%',
      table      => '*.*',
      privileges => ['ALL'],
    }
  },
}

class { 'mysql::bindings':
  php_enable => true,
}
