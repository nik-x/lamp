Following puppet modules are required for this packer build to work. 

- [apache](https://forge.puppet.com/modules/puppetlabs/apache)
- [mysql](https://forge.puppet.com/modules/puppetlabs/mysql)
- [ufw](https://forge.puppet.com/modules/domkrm/ufw)

Those modules and their dependencies need to be present in the `puppet/modules` directory.

A valid folder structure would look like this:

```
lamp
└── puppet
    └── modules
        ├── apache
        ├── concat
        ├── mysql
        ├── stdlib
        ├── ufw
        └── README.md
```
