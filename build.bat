@ECHO off

SETLOCAL ENABLEEXTENSIONS
:: get the current parent directory and the name of this batch file
SET parent=%~dp0
SET me=%~n0
:: get the current date (as yyyy-mm-dd) and time (as hh-mm-ss)
SET cur_date=%date:~6,4%-%date:~3,2%-%date:~0,2%
SET cur_time=%time:~0,2%-%time:~3,2%-%time:~6,2%
:: create file path
IF NOT EXIST %parent%logs MKDIR %parent%logs
SET log_file=logs\packer-build_%cur_date%_%cur_time%.log
ECHO %me%: log located at '.\%log_file%'
:: enable vagrant and packer debug logging and set the log file location
SET VAGRANT_LOG=info
SET PACKER_LOG=1
SET PACKER_LOG_PATH=%parent%%log_file%
:: create an empty file at the log files location since packer does not do this automatically
TYPE nul > %PACKER_LOG_PATH%
:: execute packer build (expect the file to be named 'conf.pkr.hcl')
packer build -force conf.pkr.hcl
